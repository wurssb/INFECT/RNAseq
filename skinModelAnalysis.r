## Author: Nirupama Benis
## This script shows the steps in the analysis of RNAseq of samples from skin models infected with a GAS pathogen. Mapping of the fastq files to a reference human genome was done using the tool STAR. The normalization and the differenital analysis was done using the R package edgeR. The differentially expressed genes in both analyses were then used as input to a pathway enrichment algorithm to find over-represented pathways. More details can be found in the Methods section of the publication.

## STAR mapping

# STAR --genomeDir /home/genomeDirectory --genomeFastaFiles Homo_sapiens.GRCh38.dna.primary_assembly.fa --sjdbGTFfile gencode.v26.primary_assembly.annotation.gtf --runMode genomeGenerate --runThreadN 24
# STAR --genomeDir /home/genomeDirectory --readFilesCommand zcat --runThreadN 20 --readFilesIn /home/2016_1C3S.fastq.gz --outSAMtype BAM SortedByCoordinate

## Normalise the data
library(edgeR)
countMatrix <- allExpressionDF
colnames(countMatrix) <- gsub("^X", "", colnames(countMatrix))
countMatrix = as.data.frame(countMatrix)
countMatrix <- countMatrix[rowSums(countMatrix) != 0, ]
norm.factor <- calcNormFactors((countMatrix), method = 'TMM') #get the factor
NBdata.normed <- countMatrix*norm.factor #correct the values based on the factor
normalizedData <- cpm(NBdata.normed, normalized.lib.sizes=TRUE)

## Differential analysis: Control vs Infection
# The donor information is used as a blocking factor to remove the effect of individuals on the gene expression
designDF <- data.frame(Condition = gsub("^(.*)_(Donor[[:digit:]])_([[:alnum:]]+)_L00[[:digit:]]", "\\1", colnames(NBdata.cpm)), 
                       Donor = gsub("^(.*)_(Donor[[:digit:]])_([[:alnum:]]+)_L00[[:digit:]]", "\\2", colnames(NBdata.cpm)), 
                       Time = gsub("^(.*)_(Donor[[:digit:]])_([[:alnum:]]+)_L00[[:digit:]]", "\\3", colnames(NBdata.cpm)))

designMatrix <- model.matrix(~Donor + Condition, data = designDF)
dge <- DGEList(counts = NBdata.cpm,
               group = designDF$Condition)
dge <- estimateGLMCommonDisp(dge, designMatrix)
dge <- estimateGLMTrendedDisp(dge, designMatrix)
dge <- estimateGLMTagwiseDisp(dge, designMatrix)
fit <- glmFit(dge, designMatrix)
lrt <- glmLRT(fit, coef = "ConditionControl") #Time8h
results <- topTags(lrt, n = nrow(lrt))
allResults <- results$table
sigResults <- allResults[allResults$FDR <= 0.01, ]
sigResults <- sigResults[abs(sigResults$logFC) >= 0.7, ]

## These genes from the differential analysis were used as input in the tool available on the website Reactome.org with the database version 67. The results were downloaded in comma separated value files.
reactomeControlVsInfection <- read.csv(file = "/home/workspace/resultSigConditions.csv", as.is = T)

# The results of the pathway analysis are then filtered for the p-value
reactomeControlVsInfectionSig <- reactomeControlVsInfection[reactomeControlVsInfection$Entities.pValue < 0.05, ]
