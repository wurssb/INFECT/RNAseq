## Author: Nirupama Benis
## This script shows the steps in the analysis of RNAseq of biopsies from NSTI patients data infected with a GAS pathogen. Mapping of the fastq files to a reference human genome was done using the tool STAR. The normalization and the differenital analysis was done using the R package edgeR by iterating several times over the 25 infected samples, selecting 3 to compare against the 3 healthy samples. The differentially expressed genes in at least half of the iterations were then used as input to a pathway enrichment algorithm to find over-represented pathways. More details can be found in the Methods section of the publication.

## STAR mapping

# STAR --genomeDir /home/genomeDirectory --genomeFastaFiles Homo_sapiens.GRCh38.dna.primary_assembly.fa --sjdbGTFfile gencode.v26.primary_assembly.annotation.gtf --runMode genomeGenerate --runThreadN 24
# STAR --genomeDir /home/genomeDirectory --readFilesCommand zcat --runThreadN 20 --readFilesIn /home/2016_1C3S.fastq.gz --outSAMtype BAM SortedByCoordinate

## Normalize the data
library(edgeR)
readCountMatrix <- humanCounts[rowSums(humanCounts) != 0, ]
readCountMatrix <- readCountMatrix[rowSums(readCountMatrix) != 0, ]
norm.factor <- calcNormFactors((readCountMatrix), method = 'TMM') #get the factor
NBdata.normed <- readCountMatrix*norm.factor #correct the values based on the factor
normalizedData <- cpm(NBdata.normed, normalized.lib.sizes=TRUE)

## Filter the data
filtGene <- NULL
numSamples <-NULL
for (gene in rownames(normalizedData)) { 
  tmpRow <- normalizedData[rownames(normalizedData) %in% gene, ]
  thresholdRow <- tmpRow >= 100
  numSamples <- c(numSamples, sum(thresholdRow))
  if (sum(thresholdRow) > 3) {
    filtGene <- c(filtGene, gene)
  }
}
filteredData <- normalizedData[rownames(normalizedData) %in% filtGene, ]

## Differential gene analysis
# Get the design matrix ready
designDF <- data.frame(Condition = c(rep("Biopsy", 3), rep("Control", 3)))
designMatrix <- model.matrix(~Condition, data = designDF)
# Significance thresholds for adjusted p-vaules and fold change
fdrCutoff <- 0.01
foldChangeCutoff <- 0.7
# Perform the differenital analysis 1000 times each time sampling three human infected samples against three healthy samples
sigResultsList <- list()
randNums <- list()
dataToBeReduced <- filtGeneData[, grep("Control", colnames(filtGeneData), invert = T)]
wholeData <- filtGeneData[, grep("Control", colnames(filtGeneData))]
for (i in 1:1000) { 
  # Select three samples from biopsies
  randNums[[i]] <- sample(1:length(dataToBeReduced), size = 3) 
  dataForDiffAnalysis <- cbind.data.frame(dataToBeReduced[, randNums[[i]]], wholeData)
  # Perform the differential analysis
  dge <- DGEList(counts = dataForDiffAnalysis,
                 group = designDF$Condition)
  dge <- estimateGLMCommonDisp(dge, designMatrix)
  dge <- estimateGLMTrendedDisp(dge, designMatrix)
  dge <- estimateGLMTagwiseDisp(dge, designMatrix)
  fit <- glmFit(dge, designMatrix)
  lrt <- glmLRT(fit, coef = "ConditionControl")
  results <- topTags(lrt, n = nrow(lrt))
  allResults <- results$table
  # Get only significantly differentially expressed genes
  sigResults <- allResults[allResults$FDR <= fdrCutoff, ]
  sigResults <- sigResults[abs(sigResults$logFC) >= foldChangeCutoff, ]
  sigResultsList[[i]] <- sigResults
}
# Get the genes significant in at least 500 iterations
genesSigResults <- lapply(sigResultsList, function(x) rownames(x))
allGenes <- unlist(genesSigResults)
tableAllGenes <- table(allGenes)[order(table(allGenes))]
sigGenesList <- list()
for (i in seq(from = 500, to = 950, by = 50)) {
  sigGenesList[[as.character(i)]] <- names(tableAllGenes[tableAllGenes >= i])
}

## These genes were used as input in the tool available on the website Reactome.org. The results of the pathway analysis are then filtered for the p-value
reactomeResults <- read.csv(file = "/home/workspace/reactSigGenes500StrepAUnqV67.csv", as.is = T)
reactomeResultsSig <- reactomeResults[reactomeResults$Entities.pValue < 0.01, ]
