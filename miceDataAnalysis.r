## Author: Nirupama Benis
## This script shows the steps in the analysis of RNAseq of mice data from genetically modified mice infected with a GAS pathogen. Mapping of the fastq file to a reference mouse genome was done using the tool STAR. The normalization and the differenital analysis was done using the R package edgeR. The groups of genetically modified mice were compared against wild-type mice. The differentially expressed genes were then used as input to a pathway enrichment algorithm to find over-represented pathways.

## STAR mapping
# First join the lanes  
# cat 15-023-033_TTAGGC_L003_R1_001.fastq.gz 15-023-033_TTAGGC_L004_R1_001.fastq.gz > 15-023-033_TTAGGC_R1_001.fastq.gz 
# STAR --runMode genomeGenerate --genomeFastaFiles Mus_musculus.GRCm38.dna.primary_assembly.fa --genomeDir mouseGenomeDir --sjdbGTFfile Mus_musculus.GRCm38.92.gtf  
# STAR --genomeDir mouseGenomeDir -limitBAMsortRAM 2951177089 --readFilesIn dperley/15-023-005_GATCAG_R1_001.fastq.gz --runThreadN 20 --outFileNamePrefix STAROutput/15-023-005_GATCAG --readFilesCommand zcat --quantMode GeneCounts 

# Mapping results for each of the samples were aggregated into one data frame.

## Normalise the data
library(edgeR)
countMatrix <- allExpressionDF
colnames(countMatrix) <- gsub("^X", "", colnames(countMatrix))
countMatrix = as.data.frame(countMatrix)
countMatrix <- countMatrix[rowSums(countMatrix) != 0, ]
norm.factor <- calcNormFactors((countMatrix), method = 'TMM') #get the factor
NBdata.normed <- countMatrix*norm.factor #correct the values based on the factor
normalizedData <- cpm(NBdata.normed, normalized.lib.sizes=TRUE)

## Differential analysis 
# Analysis on each of the genetic modifications against wild-type mice
conditionsTested <- table(gsub("^(.*)_[[:digit:]]{2}", "\\1", colnames(normalizedData)))[1:4]
allConditionsSigList <- list()
for (i in 1:length(conditionsTested)) { 
  dataForDiffAnalysis <- normalizedData[, c(grep(paste0("^", names(conditionsTested)[i], "_"), colnames(normalizedData)), grep("PBS", colnames(normalizedData)))]
  designDF <- data.frame(Condition = c(rep(names(conditionsTested)[i], conditionsTested[i]), 
                                       rep("PBS", 4)))
  print(dim(dataForDiffAnalysis))
  designMatrix <- model.matrix(~Condition, data = designDF)
  dge <- DGEList(counts = dataForDiffAnalysis,
                 group = designDF$Condition)
  dge <- estimateGLMCommonDisp(dge, designMatrix)
  dge <- estimateGLMTrendedDisp(dge, designMatrix)
  dge <- estimateGLMTagwiseDisp(dge, designMatrix)
  fit <- glmFit(dge, designMatrix)
  lrt <- glmLRT(fit, coef = "ConditionPBS") 
  results <- topTags(lrt, n = nrow(lrt))
  allResults <- results$table
  sigResults <- allResults[allResults$FDR <= 0.01, ]
  sigResults <- sigResults[abs(sigResults$logFC) >= 0.7, ]
  allConditionsSigList[[names(conditionsTested)[i]]] <- sigResults
}

## These genes from each of the 4 comparisons were used as input in the tool available on the website Reactome.org with the database version 67. The results were downloaded in comma separated value files.
reactomeDR14 <- read.csv(file = "/home/workspace/resultDR14vsPBS.csv", as.is = T)
reactomeDR3 <- read.csv(file = "/home/workspace/resultDR3vsPBS.csv", as.is = T)l
reactomeDR4 <- read.csv(file = "/home/workspace/resultDR4vsPBS.csv", as.is = T)
reactomeDR4DQ8 <- read.csv(file = "/home/workspace/resultDR4DQ8vsPBS.csv", as.is = T)

# The results of the pathway analysis are then filtered for the p-value
pvalueThreshold = 0.05
sigReactomeDR14 <- reactomeDR14[reactomeDR14$Entities.pValue < pvalueThreshold, ]
sigReactomeDR3 <- reactomeDR3[reactomeDR3$Entities.pValue < pvalueThreshold, ]
sigReactomeDR4 <- reactomeDR4[reactomeDR4$Entities.pValue < pvalueThreshold, ]
sigReactomeDR4DQ8 <- reactomeDR4DQ8[reactomeDR4DQ8$Entities.pValue < pvalueThreshold, ]
