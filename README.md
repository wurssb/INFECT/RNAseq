This repository contains the scripts used in the data analysis done for the paper **Distinct disease signatures in group A streptococcal necrotizing soft
tissue infections implicate anti-inflammatory macrophages**.


RNAseq data was taken from 3 systems, biopsies of NSTI patients with GAS infections, skin models infected with a strain of *Streptococcus pyogenes* isolated from a GAS infected patient and genetically modified mice infected with the same strain of *Streptococcus pyogenes*. The data analysis was harmonized for the three systems to make the results comparable. The analysis is presented in three R scripts, one for each system.
